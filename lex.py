#!/usr/bin/env python
import ply.lex as lex
import struct

t_MAGIC = r"\x45\x55\x34\x62\x69\x6e"


def unpack_to_number(s, format="<i"):
    return struct.unpack(format, s.encode('ISO-8859-1'))[0]


def read_bytes(lexer, length):
    if length < 0:
        raise RuntimeError(f"Cannot skip back in the string, we'll have loops: {length}")

    value = lexer.lexdata[lexer.lexpos:lexer.lexpos + length]
    lexer.skip(length)
    return value


def t_EQUALS(t):
    r"\x01\x00"
    t.value = "="
    return t


def t_OPEN_GROUP(t):
    r"\x03\x00"
    t.value = "("
    return t


def t_CLOSE_GROUP(t):
    r"\x04\x00"
    t.value = ")"
    return t


def t_INTEGER(t):
    r'(\x0c\x00|\x14\x00)(?s:....)'
    t.value = unpack_to_number(t.value[2:])
    return t


def t_BOOLEAN(t):
    r"((\x4B|\x4C)\x28|\x0e\x00(?s:.))"

    if t.value[0] == "\x0e":
        t.value = bool(t.value[2])
    else:
        t.value = t.value[0] == '\x4B'
    return t


def t_error(t):
    t.type = 'IDENTIFIER'
    t.lexer.skip(2)
    t.value = "".join(f"\\x{c:02x}" for c in t.value[0:2].encode("ISO-8859-1"))
    return t


def t_STRING(t):
    r'(\x0f\x00|\x17\x00)'
    length = unpack_to_number(read_bytes(t.lexer, 2), "<H")
    t.value = read_bytes(t.lexer, length)
    return t


def t_FIXED_POINT(t):
    r'\x67\x01'
    # The first 32 bits are Q16.16, the last 32 bits are unknown
    # https://stackoverflow.com/questions/44397370/fixed-point-binary-unpacking-in-python
    t.value = unpack_to_number(read_bytes(t.lexer, 4), "<i") / 2**16
    unknown = read_bytes(t.lexer, 4)
    return t


def t_FLOAT(t):
    r'\x0d\x00'
    t.value = unpack_to_number(read_bytes(t.lexer, 4), "<i") / 1000
    return t


t_ignore = r''

tokens = (
    'MAGIC',
    'EQUALS',
    'OPEN_GROUP',
    'CLOSE_GROUP',
    'NONE',
    'INTEGER',
    'BOOLEAN',
    'STRING',
    'FLOAT',
    'FIXED_POINT'
)


with open('data/Austria/game.eu4', 'rb') as fh:
    data = fh.read().decode('ISO-8859-1')

print(f"Opened game.eu4, length is {len(data)} bytes")





lexer = lex.lex()
lexer.input(data)


while True:
    t = lexer.token()
    if not t:
        break

    print(f"Found {t.type}, value '{t.value}'")

print("No more tokens")
